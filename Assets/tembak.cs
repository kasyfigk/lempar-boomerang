using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tembak : MonoBehaviour
{
    TouchJoystickRotation joy;
    public spawner spawn;
    public Rigidbody2D rigit;
    public int cek = 0;
    public int speedlaunch;
    public Transform player;
    public Transform panah;
    float angle;
    Vector2 tembak3;
    public tombol Tombol;
    public SpriteRenderer BOOM;
    public SpriteRenderer light;
    collidercode collidercode;
    public GameObject tipe;
    // Start is called before the first frame update
    void Start()
    {
        speedlaunch = 200;
        cek = 0;
        joy = GameObject.Find("panah arah").GetComponent<TouchJoystickRotation>();
        spawn = GameObject.Find("spawnpoint").GetComponent<spawner>();
        panah = GameObject.Find("panah arah").GetComponent<Transform>();
        Tombol = GameObject.Find("Canvas").GetComponent<tombol>();
        collidercode = GameObject.Find("boomerang").GetComponent<collidercode>();
        
    }
    public void tekan()
    { 
        if (!Tombol.pause)
        {
            if (spawn.popup == false)
            {
                if (cek == 0)
                {
                    if (joy.lastObjectRotation != new Vector2(0f, 0f))
                    {
                        if (!light.enabled)
                        {


                            BOOM.enabled = true;
                            soundmanager.playsound("duar");
                            rigit.simulated = true;
                            cek = 1;
                            rigit.AddForce((tembak3) * speedlaunch);
                            //Vector3 dir = Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.right;
                            // rigit.AddForce(dir * speedlaunch);
                        }
                        else
                        {
                            if (collidercode.ghostfrom == 1)
                            {
                                BOOM.enabled = true;
                                soundmanager.playsound("duar");
                                rigit.simulated = true;
                                cek = 1;
                                rigit.AddForce((tembak3) * (speedlaunch + 400));
                            }
                            else
                            {
                                BOOM.enabled = true;
                                soundmanager.playsound("duar");
                                rigit.simulated = true;
                                cek = 1;
                                rigit.AddForce((tembak3) * (speedlaunch / 2));
                            }
                        }


                    }

                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.isMobilePlatform)
        {
            tipe.SetActive(true);

        }
        else
        {
            tipe.SetActive(false);
            if (Input.GetKeyDown("space"))
            {
                tekan();
            }
        }

        if (joy.lastObjectRotation.x < -0.4)
        {
            tembak3.y = joy.lastObjectRotation.y;
            tembak3.x = joy.lastObjectRotation.x+0.3f;
        }
        else if(joy.lastObjectRotation.x >0.4)
        {
            tembak3.y = joy.lastObjectRotation.y;
            tembak3.x = joy.lastObjectRotation.x - 0.3f;
        }
        
        else if (joy.lastObjectRotation.x > 0.01 && joy.lastObjectRotation.x < 0.4)
        {
            tembak3.y = joy.lastObjectRotation.y ;
            tembak3.x = joy.lastObjectRotation.x;
        }
        else if (joy.lastObjectRotation.x < -0.01 && joy.lastObjectRotation.x > -0.4)
        {
            tembak3.y = joy.lastObjectRotation.y;
            tembak3.x = joy.lastObjectRotation.x+0.1f;
        }
        else
        {
            tembak3.y = joy.lastObjectRotation.y;
            tembak3.x = joy.lastObjectRotation.x-0.1f ;
        }
        //angle = panah.transform.eulerAngles.z;
        //Debug.Log(angle);
        //Debug.Log(joy.GameobjectRotation);
        /* if (cek == 1)
         {
         rigit.AddForce(joy.GameobjectRotation * 10);
         Debug.Log(joy.GameobjectRotation);

         }*/

    }
}
