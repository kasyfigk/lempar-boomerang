using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class tombolGameover : MonoBehaviour
{
    public TextMeshProUGUI skore;
    // Start is called before the first frame update
    void Start()
    {
        skore.text = "Score: "+PlayerPrefs.GetInt("skor").ToString() + "\n" + "HighScore: " + PlayerPrefs.GetInt("Highscore").ToString();
    }

    // Update is called once per frame
   public void restar()
    {
        SceneManager.LoadScene("Main");
    }
    public void homee()
    {
        SceneManager.LoadScene("Menu");
    }
}
