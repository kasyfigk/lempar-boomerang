using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class collidercode : MonoBehaviour
{
    private Rigidbody2D rb;
    public TextMeshProUGUI target;
    public int pengecek;
    spawner spawn;
    public tembak tembak1;
    bounce bounces;
    public Transform bomerang;
    public int nilai;
    public int waves;
    public TextMeshProUGUI wavess;
    public scoremanager scoree;
    public hit Hit;
    public Transform hitefek;
    public SpriteRenderer BOOM;
    public jalanghost jalann;
    public powerup power;
    public int ghostfrom;
    public hitmerah merah;
    public Transform hitme;
    
    
    public void Start()
    {
        nilai = 0;
        waves = 0;
        spawn = GameObject.Find("spawnpoint").GetComponent<spawner>();
        scoree = GameObject.Find("ScoreManager").GetComponent<scoremanager>();
        power = GameObject.Find("light").GetComponent<powerup>();
        bounces = GetComponent<bounce>();
        Hit = GetComponent<hit>();
        ghostfrom = 0;
        merah = GameObject.Find("kondisi").GetComponent<hitmerah>();
        

        
    }
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

    }
    // Start is called before the first frame update
    public void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("kena");
        if (other.tag == "cok")
        {
            BOOM.enabled = false;
            hitefek.position = bomerang.position;
            soundmanager.playsound("lazer");
            Destroy(other.gameObject);
            Hit.sethit();
            bomerang.position = new Vector3(0f, -3.934f, 0f);
            tembak1.cek = 0;
            bounces.tabrak = 0;
            rb.velocity = new Vector3(0f, 0f, 0f);
            rb.simulated = false;
            nilai++;
            scoree.nambahskor(10);


        }

        if (other.tag == "cuk")
        {
            if ((other.gameObject.GetComponent<dragonjalan>().hp - 1) >= 1)
            {

                BOOM.enabled = false;
                hitefek.position = bomerang.position;
                soundmanager.playsound("lazer");
                
                Hit.sethit();
                bomerang.position = new Vector3(0f, -3.934f, 0f);
                tembak1.cek = 0;
                bounces.tabrak = 0;
                rb.velocity = new Vector3(0f, 0f, 0f);
                rb.simulated = false;
                other.gameObject.GetComponent<dragonjalan>().hp -= 1;
                scoree.nambahskor(5);
            }
            else
            {
                BOOM.enabled = false;
                hitefek.position = bomerang.position;
                soundmanager.playsound("lazer");
                Destroy(other.gameObject);
                
                Hit.sethit();
                bomerang.position = new Vector3(0f, -3.934f, 0f);
                tembak1.cek = 0;
                bounces.tabrak = 0;
                rb.velocity = new Vector3(0f, 0f, 0f);
                rb.simulated = false;
                nilai++;
                scoree.nambahskor(10);

            }
        }



        if (other.tag == "ghost") 
        { 

        if (other.gameObject.GetComponent<jalanghost>().pilihan == 2)
            {
             soundmanager.playsound("tik");
                ghostfrom = 1;
                hitme.position = bomerang.position;
                merah.sethitm();
                power.setpowah();
             Destroy(other.gameObject);
             scoree.nambahskor(30);
             }
            else
            {
                ghostfrom = 2;
                hitme.position = bomerang.position;
                merah.sethitm();
                soundmanager.playsound("tik");
                power.setpowah();
                Destroy(other.gameObject);
                scoree.nambahskor(5);
            }
        //SceneManager.LoadScene("menu");
        }


    }
    public void Update()
    {
        
        //wavess.text = "waves: " + waves.ToString()+" | 3";
        pengecek = (spawn.level + 2) ;
        target.text =  "X "+(pengecek - nilai).ToString();
       /* if (waves >= 3)
        {
            nilai = 0;
            waves = 0;
            spawn.waktu();
          
        }
        else {
            if (nilai >= pengecek)
            {
                nilai = 0;
                waves++;
                spawn.wave();
                
    

             }   

         }*/
       if (nilai >= pengecek)
        {
            nilai = 0;
            spawn.waktu();
        }
    }
}
