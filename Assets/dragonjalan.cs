using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dragonjalan : MonoBehaviour
{
    
    float _rand;
    [SerializeField] Transform M_player;
    public darah darah;
    UnityEngine.AI.NavMeshAgent agent;
    public collidercode col;
    public Transform tes;
    float x;
    float y;
    Vector2 lastpos;
    listpoint listpoint;
    [SerializeField]  int waypointIndex;
    public int hp;
    SpriteRenderer warna;
    float randomm;
    spawner spawn;
    float imspeed;
    float randomjalan;

    // Start is called before the first frame update
    void Start()
    {
        //agent = GetComponent<NavMeshAgent>();
        // agent.updateRotation = true;
        // agent.updateUpAxis = true;
        M_player = GameObject.Find("run_0").GetComponent<Transform>();
        darah = GameObject.Find("kondisi").GetComponent<darah>();
        col = GameObject.Find("boomerang").GetComponent<collidercode>();
        listpoint = GameObject.Find("waypoint").GetComponent<listpoint>();
        spawn = GameObject.Find("spawnpoint").GetComponent<spawner>();
        waypointIndex = 0;
        warna = GetComponent<SpriteRenderer>();
        randomjalan = Random.Range(0, 100); 
        if(spawn.level >= 10 && spawn.level <=20)
        {
            randomm = Random.Range(0, 100);
            if (randomm < 50)
            {
                hp = 3;
                warna.color = new Color(1f, 1f, 1f);
                imspeed = 1f;
            }
            else
            {
                hp = 2;
                warna.color = new Color(0f, 1f, 0.8f);
                imspeed = 2f;
            }
        }
        else if (spawn.level >=20)
        {
            randomm = Random.Range(0, 100);
            if (randomm < 35)
            {
                hp = 3;
                warna.color = new Color(1f, 1f, 1f);
                imspeed = 1f;
            }
            else if (randomm <=70 && randomm >35)
            {
                hp = 2;
                warna.color = new Color(0f, 1f, 0.8f);
                imspeed = 2f;
            }
            else
            {
                hp = 5;
                warna.color = new Color(0f, 1f, 0.07f);
                imspeed = 0.6f;
            }
        }
        else
        {
            hp = 3;
            warna.color = Color.white;
            imspeed = 1f;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
        if (randomjalan <= 50)
        {
            x = listpoint.point[waypointIndex].position.x - transform.position.x;
            y = listpoint.point[waypointIndex].position.y - transform.position.y;
            float angle = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, angle + 270);
            if (waypointIndex  < 5)
            {
                if (transform.position.y == listpoint.point[waypointIndex].position.y)
                {

                    waypointIndex++;
                }
                else
                {
                    transform.position = Vector2.MoveTowards(transform.position, listpoint.point[waypointIndex].position, imspeed * Time.deltaTime);
                }
            }
        }
        else
        {
            x = listpoint.point[waypointIndex + 5].position.x - transform.position.x;
            y = listpoint.point[waypointIndex + 5].position.y - transform.position.y;
            float angle = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, angle + 270);
            if (waypointIndex + 5 < 10)
            {
                if (transform.position.y == listpoint.point[waypointIndex+5].position.y)
                {

                    waypointIndex++;
                }
                else
                {
                    transform.position = Vector2.MoveTowards(transform.position, listpoint.point[waypointIndex+5].position, imspeed * Time.deltaTime);
                }
            }
        }

        //transform.position = Vector2.MoveTowards(transform.position, M_player.position, 1f * Time.deltaTime);
        //Debug.Log("tujuan: "+agent.nextPosition+"  current : "+transform.position);

        //agent.SetDestination(M_player.position);
        //float angle = Vector3.Angle(agent.velocity.normalized, this.transform.forward);
        //float angle = (Mathf.Atan2(tes.position.y, tes.position.x) * Mathf.Rad2Deg) ;
        //transform.localRotation = Quaternion.Euler(0f, 0f, angle);
        // float distance = Vector3.Distance(transform.position, M_player.position);




        //float input = Mathf.Clamp01(muter.magnitude);

        // float angle = (Mathf.Atan2(agent.nextPosition.y, agent.nextPosition.x) * Mathf.Rad2Deg);
        // Debug.Log(angle);
        //Quaternion angles = Quaternion.AngleAxis(angle, Vector3.forward);
        //transform.rotation = Quaternion.Slerp(transform.rotation, angles, Time.deltaTime*9999999999);
        //transform.position = Vector3.MoveTowards(transform.position, M_player.position, 1f * Time.deltaTime);

        //Vector2 move = new Vector2(M_player.position.x, M_player.position.y);
        //move.Normalize();
        //if(move!= Vector2.zero)
        //{
        // Quaternion torotate = Quaternion.LookRotation(Vector3.forward, move);
        //transform.rotation = torotate;
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, torotate, 100f * Time.deltaTime);

        //}
        if (transform.position.y <= M_player.position.y + 0.5)
        {
           Destroy(gameObject);
            darah.kurangnyawa();
           col.nilai++;
       }
    }
}
