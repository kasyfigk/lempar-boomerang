using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class textpopup : MonoBehaviour
{
    // Start is called before the first frame update
    public float showspriteb;
    public GameObject pop;
    public TextMeshProUGUI popupp;
    public spawner spawn;
    // Start is called before the first frame update
    public void Start()
    {
        spawn = GameObject.Find("spawnpoint").GetComponent<spawner>();
        
        popupp.text = "WAVES: " + spawn.level.ToString();
    }
    public void setpop()
    {
        showspriteb = 2f;
    }

    // Update is called once per frame
    public void Update()
    {
        popupp.text = "WAVES: "+spawn.level.ToString();
        if (showspriteb > 0)
        {

            pop.SetActive(true);


        }
        else
        {
            pop.SetActive(false);
        }
        showspriteb -= Time.deltaTime;
    }
    public IEnumerator popopop()
    {
        
        setpop();
        yield return new WaitForSeconds(0);
        
    }
}
