using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trajectory : MonoBehaviour
{
    TouchJoystickRotation joy;
    [SerializeField] LineRenderer linee;
    [SerializeField] Transform panah;
    RaycastHit2D hit;
    int totalbounce = 1;
    public float LineOffset = 0.01f;
    public bounce bounces;
    public int banyaktraject = 0; 
    
    
    // Start is called before the first frame update
    void Start()
    {
        banyaktraject = 1;
        joy = GameObject.Find("panah arah").GetComponent<TouchJoystickRotation>();
        bounces = GameObject.Find("boomerang").GetComponent<bounce>();
    }

    // Update is called once per frame
    void Update()
    { 
        totalbounce = banyaktraject;
        linee.positionCount = totalbounce + 1;
        Vector2 direction = transform.right;
        Vector2 origin = (Vector2)transform.position + LineOffset * direction;
        if (joy.GameobjectRotation != new Vector2(0f, 0f))
        {
            Vector3[] simpan = new Vector3[totalbounce+1];
            linee.enabled = true;

            //linee.SetPosition(0, origin);
            // linee.SetPosition(1, hit.point);

            simpan[0] = origin;
            for (int i = 1; i <= totalbounce; i++)
            {
                hit = Physics2D.Raycast(origin, direction);
                simpan[i] = hit.point;
                //Debug.DrawLine(origin, hit.point);

                if (hit.transform.tag != "cok"  )
                {
                    direction = Vector2.Reflect(direction.normalized, hit.normal);
                    origin = hit.point + LineOffset * direction;
                }
                if(hit.transform.tag != "cuk")
            {
                direction = Vector2.Reflect(direction.normalized, hit.normal);
                origin = hit.point + LineOffset * direction;
            }
                if(hit.transform.tag != "ghost")
                {
                    direction = Vector2.Reflect(direction.normalized, hit.normal);
                    origin = hit.point + LineOffset * direction;
                }




            }
            linee.SetPositions(simpan);
        }
        else
        {
            linee.enabled = false;
        }
    }
    public void trajectoryplus()
    {
        banyaktraject++;
    }
}

