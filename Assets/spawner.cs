using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class spawner : MonoBehaviour
{   public TextMeshProUGUI textlevel;
    public Transform[] m_spawnpoints;
    public GameObject[] m_enemyPrefabs;
    public float m_BesarObjek = 0.1f;
    public float m_BesarObjek1 = 0.1f;
    int _randSpawPoint;
    float  waktuspawn;
    int _randEnemy;
    int _cek;
    public float delaykata = 2f;
    public float delaykata1 = 15f;
    private float timerkataselanjutnya = 0f;
    public int level;
    public int batasspawn;
    public bool popup;
    public float randomchange;
    public float randomchange1;
    public textpopup pop;
    float timeghost = 10f;
    public int pilihkirikananghost;
    float randomm;
    public float random1;
    
    // Start is called before the first frame update
    void Start()
    {
        pop = GameObject.Find("kondisi").GetComponent<textpopup>();
        waktuspawn = 3;
        level = 1;
        batasspawn = 1;
        popup = false;
        randomchange = 0;
        randomchange1 = 0;
        StartCoroutine(pop.popopop());
        

    }

    public void Update()
    {
        
        textlevel.text = "Waves: " + level.ToString();
        if (Time.time >= timerkataselanjutnya)
        {
            if (popup==false)
            {
                if (batasspawn <= (level + 2) )
                {
                    float random9 = Random.Range(0, 100);
                    if (level < 5)
                    {
                        if (random9 <= 50)
                        {

                            //int _randEnemy = Random.Range(0, m_enemyPrefabs.Length - 1);
                            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length - 2);
                            GameObject newobject = Instantiate(m_enemyPrefabs[0], m_spawnpoints[_randSpawPoint].position, transform.rotation);
                            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
                            //newobject.transform.rotation = Quaternion.Euler(0f, 0f, -180f);
                            batasspawn++;
                            timerkataselanjutnya = Time.time + delaykata;
                            delaykata *= .99f;
                        }
                        else
                        {
                            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length - 2);
                            GameObject newobject = Instantiate(m_enemyPrefabs[1], m_spawnpoints[_randSpawPoint].position, transform.rotation);
                            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
                            //newobject.transform.rotation = Quaternion.Euler(0f, 0f, -180f);
                            batasspawn++;
                            timerkataselanjutnya = Time.time + delaykata;
                            delaykata *= .99f;
                        }
                    }
                    else
                    {
                        if (random9 <= 39)
                        {

                            //int _randEnemy = Random.Range(0, m_enemyPrefabs.Length - 1);
                            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length - 2);
                            GameObject newobject = Instantiate(m_enemyPrefabs[0], m_spawnpoints[_randSpawPoint].position, transform.rotation);
                            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
                            //newobject.transform.rotation = Quaternion.Euler(0f, 0f, -180f);
                            batasspawn++;
                            timerkataselanjutnya = Time.time + delaykata;
                            delaykata *= .99f;
                        }
                        else if(random9 >= 61)
                        {
                            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length - 2);
                            GameObject newobject = Instantiate(m_enemyPrefabs[1], m_spawnpoints[_randSpawPoint].position, transform.rotation);
                            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
                            //newobject.transform.rotation = Quaternion.Euler(0f, 0f, -180f);
                            batasspawn++;
                            timerkataselanjutnya = Time.time + delaykata;
                            delaykata *= .99f;
                        }
                        else
                        {
                            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length - 2);
                            GameObject newobject = Instantiate(m_enemyPrefabs[2], m_spawnpoints[_randSpawPoint].position, transform.rotation);
                            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
                            //newobject.transform.rotation = Quaternion.Euler(0f, 0f, -180f);
                            batasspawn++;
                            timerkataselanjutnya = Time.time + delaykata;
                            delaykata *= .99f;
                        }
                    }

                }
            }

        }
        if (Time.time >= timeghost)
        {
            
            randomm = Random.Range(0, 100);
            if (popup == false)
            {
                random1 = Random.Range(0, 100);
                if (random1 <= 45)
                {
                    if (randomm <= 50)
                    {

                        //int _randEnemy = 1;
                        int _randSpawPoint1 = 4;
                        pilihkirikananghost = _randSpawPoint1;
                        GameObject newobject = Instantiate(m_enemyPrefabs[3], m_spawnpoints[_randSpawPoint1].position, transform.rotation);
                        newobject.transform.localScale = new Vector3(m_BesarObjek1, m_BesarObjek1, m_BesarObjek1);
                        timeghost = Time.time + delaykata1;
                    }
                    else
                    {
                        int _randSpawPoint1 = 5;
                        pilihkirikananghost = _randSpawPoint1;
                        GameObject newobject = Instantiate(m_enemyPrefabs[3], m_spawnpoints[_randSpawPoint1].position, transform.rotation);
                        newobject.transform.localScale = new Vector3(m_BesarObjek1, m_BesarObjek1, m_BesarObjek1);
                        timeghost = Time.time + delaykata1;
                    }
                }
                else
                {
                    timeghost = Time.time + delaykata1;
                }
                
                
            }
        }

        

    }
    public void waktu()
    {
        randomchange = Random.Range(0, 100);
        randomchange1 = Random.Range(0, 100);
        level++;
        batasspawn = 1;
        popup = true;
       
        
        
    }
    public void wave()
    {
        randomchange = Random.Range(0, 100);
        randomchange1 = Random.Range(0, 100);
        batasspawn = 1;
        popup = true;
        
    }
    

}
