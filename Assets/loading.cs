using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loading : MonoBehaviour
{
    public Transform muter;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loadd());
    }

    // Update is called once per frame
    void Update()
    {
        muter.Rotate(0f, 0f, 200f*Time.deltaTime);
    }
    IEnumerator loadd()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Main");
    }
}
