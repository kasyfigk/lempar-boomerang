using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class scoremanager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI skor;
    public int score;
    public int highscore;
    public TextMeshProUGUI hg;
    public TextMeshProUGUI scr;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        skor.text = score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        skor.text = "Score: "+score.ToString();
        hg.text ="HighScore: "+PlayerPrefs.GetInt("Highscore").ToString();
        scr.text = "Score: " + score.ToString();
    }
    public void nambahskor(int _tambah)
    {
        score += _tambah;
        if(highscore < score)
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }

}
