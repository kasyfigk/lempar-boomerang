using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class tombol : MonoBehaviour
{
    public GameObject imgpause;
    public bool pause;
    public bool hTp;
    public GameObject htp;
    public void Home()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("menu");

    }
    public void Pause()
    {
        if (!pause)
        {
            imgpause.SetActive(true);
            Time.timeScale = 0f;
            pause = true;

        }
        else
        {
            imgpause.SetActive(false);
            Time.timeScale = 1f;
            pause = false;

        }
    }
    public void HtP()
    {
        if (!hTp)
        {
            htp.SetActive(true);
            hTp = true;

        }
        else
        {
            htp.SetActive(false);
            hTp = false;

        }
    }
}
