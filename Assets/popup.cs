using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class popup : MonoBehaviour
{
    public spawner spawn;
    
    public GameObject pilihan;
    public bounce bounces;
    public tembak tembak1;
    public trajectory trajec;
    public darah Darah;
    public TextMeshProUGUI textkiri;
    public TextMeshProUGUI textkanan;
    public textpopup pop;
    // Start is called before the first frame update

    void Start()
    {
        spawn = GameObject.Find("spawnpoint").GetComponent<spawner>();
        bounces = GameObject.Find("boomerang").GetComponent<bounce>();
        tembak1 = GameObject.Find("boomerang").GetComponent<tembak>();
        trajec = GameObject.Find("panah arah").GetComponent<trajectory>();
        Darah = GameObject.Find("kondisi").GetComponent<darah>();
        pop = GameObject.Find("kondisi").GetComponent<textpopup>();
        
    }
    // Update is called once per frame
    void Update()
    {
        if (spawn.popup)
        {
            pilihan.SetActive(true);
            if(spawn.randomchange < 50)
            {
                textkanan.text = "Bounce +1";
            }
            else
            {
                textkanan.text = "Trajectory  +1";
            }
            if (spawn.randomchange1 < 50)
            {
                textkiri.text = "Force Speed ++";
            }
            else
            {
                textkiri.text = "Health +1";
            }
        }
        else
        {
            pilihan.SetActive(false);
        }
    }
    public void tabrak1()
    {
        if (spawn.randomchange < 50)
        {
            bounces.banyaktabrak++;
            spawn.popup = false;
            pop.setpop();
        }
        else
        {
            trajec.trajectoryplus();
            spawn.popup = false;
            pop.setpop();
        }

    }
    public void speed1()
    {
        if (spawn.randomchange1 < 50)
        {
            tembak1.speedlaunch = tembak1.speedlaunch + 25;
            spawn.popup = false;
            pop.setpop();
        }
        else
        {
            Darah.M_nyawa++;
            spawn.popup = false;
            pop.setpop();
        }
    }
}
