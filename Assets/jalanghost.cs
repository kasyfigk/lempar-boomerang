using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jalanghost : MonoBehaviour
{
    public spawner spawn;
    public Transform kiri;
    public Transform kanan;
    public int anyar;
    public float random1;
    SpriteRenderer spriteRenderer;
    public scoremanager scoree;
    public int pilihan;
    // Start is called before the first frame update
    void Start()
    {
        random1 = Random.Range(0, 100);
        spawn = GameObject.Find("spawnpoint").GetComponent<spawner>();
        kiri = GameObject.Find("spam5").GetComponent<Transform>();
        kanan = GameObject.Find("spam6").GetComponent<Transform>();
        anyar = spawn.pilihkirikananghost;
        spriteRenderer = GetComponent<SpriteRenderer>();
        scoree = GameObject.Find("ScoreManager").GetComponent<scoremanager>();
        



    }

    // Update is called once per frame
    void Update()
    {
        
        if (anyar == 4)
        {
            float xPos = Mathf.Sin(Time.time);
            transform.position = new Vector2(transform.position.x - 1f*Time.deltaTime, xPos );
            if (transform.position.x <= kanan.transform.position.x)
            {
                Destroy(gameObject);
            }

        }
        else

        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            float xPos = Mathf.Sin(Time.time);
            transform.position = new Vector2(transform.position.x + 1f * Time.deltaTime, xPos);
            if (transform.position.x >= kiri.transform.position.x)
            {
                Destroy(gameObject);
            }
        }
        if (random1 >= 50)
      {
            spriteRenderer.color = new Color(1f, 0.37f, 0.37f);
            pilihan = 1;
       }
       else
       {
            spriteRenderer.color = Color.white;
            pilihan = 2;
       }
    }
    

}
