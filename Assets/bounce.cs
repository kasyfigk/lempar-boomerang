using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bounce : MonoBehaviour
{
    private Rigidbody2D rb;
    Vector3 lastVelocity;
    public tembak tembak1;
    public int tabrak;
    public Transform bomerang;
    public int banyaktabrak;
    public SpriteRenderer BOOM;
    public spark spark;
    public Transform sparkk;

    private void Start()
    {
        banyaktabrak = 2;
       
          }
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

    }
    private void Update()
    {
        lastVelocity = rb.velocity;
        
        if (tabrak >= banyaktabrak)
        {
            sparkk.position = transform.position;
            spark.sethitv();
            BOOM.enabled = false;
            soundmanager.playsound("blackh");
            bomerang.position = new Vector3(0f, -3.934f, 0f);
            tembak1.cek = 0;
            tabrak = 0;
            rb.velocity = new Vector3(0f,0f,0f);
            rb.simulated = false;

        }
       
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        sparkk.position = transform.position;
        spark.sethitv();
        var speed = lastVelocity.magnitude;
        var direction = Vector3.Reflect(lastVelocity.normalized, collision.contacts[0].normal);
        rb.velocity = direction * Mathf.Max(speed, 0f);
        soundmanager.playsound("tembak");
        tabrak++;
    }

}
