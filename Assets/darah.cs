using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class darah : MonoBehaviour
{
    public scoremanager scoree;
    public TextMeshProUGUI nyawa; 
    public TextMeshProUGUI nyawa1;
    public int M_nyawa;
    // Start is called before the first frame update
    void Start()
    {
        M_nyawa = 5;
        scoree = GameObject.Find("ScoreManager").GetComponent<scoremanager>();
    }
    public void kurangnyawa()
    {
        M_nyawa--;
    }

    // Update is called once per frame
    void Update()
    {
        nyawa.text ="Health: "+ M_nyawa.ToString();
        nyawa1.text = "X " + M_nyawa.ToString();
        if (M_nyawa <= 0)
        {
            PlayerPrefs.SetInt("skor", scoree.score);
            SceneManager.LoadScene("Gameover");

        }
    }
}
